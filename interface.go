package main

import (
	"fmt"
	"tutorial/libraries"
	"tutorial/models"
)

func interfaceTest() {
	var secret interface{}

	secret = "ethan hunt"
	fmt.Println(secret)

	secret = []string{"apple", "manggo", "banana"}
	fmt.Println(secret)

	secret = 12.4
	fmt.Println(secret)

	libraries.SayHello()

	var sapi = models.Hewan{Nama: "Sapi", Umur: 13, Kelamin: "Jantan"}
	fmt.Println(sapi.Nama)

	var data map[string]interface{}

	data = map[string]interface{}{
		"name":      "ehtan hunt",
		"grade":     2,
		"breakfast": []string{"apple", "manggo", "banana"},
	}

	fmt.Println(data)
}
