package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

type student struct {
	id    string
	name  string
	age   int
	grade int
}

func connect() (*sql.DB, error) {
	db, err := sql.Open("mysql", "gotutor:rahasia@tcp(127.0.0.1:3306)/gotutor")

	if err != nil {
		fmt.Println("Error!", err)
		return nil, err
	}

	return db, nil
}

func sqlQueryRow() {
	var db, err = connect()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	defer db.Close()

	var result = student{}

	var id = "E001"
	err = db.QueryRow("select name, grade from tb_student where id = ?", id).Scan(&result.name, &result.age)

	if err != nil {
		fmt.Println("Terjadi error!", err.Error())
		return
	}

	fmt.Printf("name: %s\ngrade: %d\n", result.name, result.age)
}

func sqlQuery() {
	db, err := connect()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	defer db.Close()

	var age = 30
	rows, err := db.Query("select id, name, age from tb_student where age = ?", age)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	defer rows.Close()

	var result []student

	for rows.Next() {
		var each = student{}
		var err = rows.Scan(&each.id, &each.name, &each.grade)

		if err != nil {
			fmt.Println(err.Error())
			return
		}

		result = append(result, each)
	}

	if err = rows.Err(); err != nil {
		fmt.Println(err.Error())
		return
	}

	if len(result) == 0 {
		fmt.Println("No data found")
		return
	}

	for _, each := range result {
		fmt.Println(each.name)
	}
}

func sqlExec() {
	db, err := connect()
	if err != nil {
		fmt.Println("Terjadi error", err.Error())
		return
	}

	defer db.Close()

	_, err = db.Exec("insert into tb_student values(?,?,?,?)", "G001", "Galahad", 29, 2)

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("insert success!")

	_, err = db.Exec("update tb_student set age = ? where id = ?", 28, "G001")

	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("update success!")

	_, err = db.Exec("delete from tb_student where id = ? ", "G001")
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("delete success")

}

func database() {
	// sqlQuery()
	// sqlQueryRow()
	sqlExec()
}
