package main

import (
	"fmt"
	"math/rand"
	"runtime"
	"time"
)

func unbuffered() {
	runtime.GOMAXPROCS(2)

	messages := make(chan int, 2)

	go func() {
		for {
			i := <-messages
			fmt.Println("received data:", i)
		}
	}()

	for i := 0; i < 5; i++ {
		fmt.Println("send data", i)
		time.Sleep(time.Duration(rand.Intn(2)) * time.Second)
		messages <- i
	}
}
