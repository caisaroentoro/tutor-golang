package main

import (
	"crypto/sha1"
	"fmt"
	"time"
)

func hash() {
	var text = "this is a secret"
	var sha = sha1.New()

	sha.Write([]byte(text))
	var encrypted = sha.Sum(nil)
	var encryptedString = fmt.Sprintf("%x", encrypted)

	fmt.Println(encryptedString)

	var name = "caisar"

	enkripsi, salt := doHashUsingSalt(name)

	fmt.Println("Encrypted: ", enkripsi)
	fmt.Println("salt:", salt)

}

func doHashUsingSalt(text string) (string, string) {
	var salt = fmt.Sprintf("%d", time.Now().UnixNano())
	var saltedText = fmt.Sprintf("text: '%s', salt: %s", text, salt)
	fmt.Println(saltedText)
	var sha = sha1.New()
	sha.Write([]byte(saltedText))
	var encrypted = sha.Sum(nil)

	return fmt.Sprintf("%x", encrypted), salt
}
