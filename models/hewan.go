package models

//Hewan adalah abstraksi dari hewan
type Hewan struct {
	Nama    string
	Umur    int
	Kelamin string
}
