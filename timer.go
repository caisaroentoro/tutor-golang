package main

import (
	"fmt"
	"time"
)

func timer() {
	fmt.Println("start")
	time.Sleep(time.Second * 4)
	fmt.Println("after 4 seconds")

	// for true {
	// 	fmt.Println("Hello!! ")
	// 	time.Sleep(1 * time.Second)
	// }

	var ch = make(chan bool)

	time.AfterFunc(4*time.Second, func() {
		fmt.Println("expired")
		ch <- true
	})

	fmt.Println("start")
	<-ch
	fmt.Println("finish")
}
