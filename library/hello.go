package library

import "fmt"

//SayHello to other
func SayHello() {
	fmt.Println("Hello world")
}

//introduce your name to other
func introduce(name string) {
	fmt.Println("nama saya", name)
}
