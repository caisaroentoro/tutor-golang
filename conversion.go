package main

import (
	"fmt"
	"strconv"
)

func conversion() {
	var num = 123
	var str = strconv.Itoa(num)

	fmt.Println(str)

	var numstr = "123"
	var numint, err = strconv.ParseInt(numstr, 10, 64)

	if err == nil {
		fmt.Println(numint)
	}

	var binstr = "01111"
	binint, err := strconv.ParseInt(binstr, 2, 8)

	if err == nil {
		fmt.Println(binint)
	}

}
