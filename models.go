package main

import (
	"fmt"
	"tutorial/models"
)

func model_test() {
	hewan := models.Hewan{
		Nama:    "Anjing",
		Umur:    29,
		Kelamin: "Jantan",
	}

	fmt.Println(hewan.Nama)
}
