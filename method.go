package main

import "fmt"

import "strings"

type student struct {
	name  string
	grade int
}

//method struct
func (s student) sayHello() {
	fmt.Println("hallo ", s.name)
}

//method struct
func (s student) getName(i int) string {
	return strings.Split(s.name, " ")[i-1]
}

//change name using method struct
func (s student) changeName(name string) {
	fmt.Println("----> on changeName, name changed to: ", name)
	s.name = name
}

//change name using pointer on method struct
func (s *student) changeName1(name string) {
	fmt.Println("----> on changeName, name changed to: ", name)
	s.name = name
}

func test() {
	var s1 = student{"john wick", 21}
	s1.sayHello()
	var name = s1.getName(2)
	fmt.Println("nama panggilan: ", name)

	s1.changeName("jason bourne")
	fmt.Println("s1 after change name: ", s1.name)

	s1.changeName1("ethan hunt")
	fmt.Println("s1 after change name1: ", s1.name)

	s1.sayHello()

	// fmt.Println(strings.Split(s)
}
