package main

import (
	"fmt"
)

type User struct {
	Fullname string `json:"Name"`
	Age      int
}

func json() {
	var jsonString = `{"Name": "john wick", "age":27}`
	var jsonData = []byte(jsonString)

	var data1 map[string]interface{}
	json.Unmarshal(jsonData, &data1)

	var data User

	var err = json.Unmarshal(jsonData, &data)
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	fmt.Println("user :", data.Fullname)
	fmt.Println("age :", data.Age)

	fmt.Println("user ", data1["Name"])
	fmt.Println("age :", data1["age"])

	var jsonString2 = `[
			{"Name":"john wick", "age":27},
			{"Name":"ethan hunt", "age":32}
		]`

	var data2 []User

	var err2 = json.Unmarshal([]byte(jsonString2), &data2)

	if err2 != nil {
		fmt.Println(err2.Error())
		return
	}
	fmt.Println(data2[0].Fullname)
	fmt.Println(data2[1].Fullname)

	var object = []User{{"Caisar Oentoro", 28}, {"Dirda Muthi", 28}}
	var jsonData3, err3 = json.Marshal(object)

	if err != nil {
		fmt.Println(err3.Error())
	}

	var jsonString3 = string(jsonData3)
	fmt.Println(jsonString3)
}
