package main

import "fmt"

type student_print struct {
	name        string
	height      float64
	age         int32
	isGraduated bool
	hobbies     []string
}

var data = student_print{
	name:        "wick",
	height:      182.5,
	age:         26,
	isGraduated: false,
	hobbies:     []string{"eating", "sleeping"},
}

func format_print() {
	fmt.Printf("%b\n", data.age)
	fmt.Printf("%c\n", 1235)

	fmt.Printf("%#v\n", data)
}
