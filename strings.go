package main

import (
	"fmt"
)

func strings() {
	var isExists = strings.Contains("John wick", "wick")
	fmt.Println(isExists)

	var text = "banana"
	var find = "a"
	var replaceWith = "o"

	var newText = strings.Replace(text, find, replaceWith, -1)

	fmt.Println(newText)

	var data = []string{"caisar", "oentoro"}

	var caisaroentoro = strings.Join(data, " ")

	fmt.Println(caisaroentoro)

	alay := "alAy"
	var str = strings.ToLower(alay)
	fmt.Println(str)

}
