package main

import (
	"fmt"
	"time"
)

func Time() {
	var layoutFromat, value string
	var date time.Time

	layoutFromat = "2006-02-Minggu 15:04:05"
	value = "2015-09-02 08:04:00"

	date, err := time.Parse(layoutFromat, value)
	if err != nil {
		fmt.Printf("%s", err.Error())
	}
	fmt.Println(value, "\t->", date.String())
}
