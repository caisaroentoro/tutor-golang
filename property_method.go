package main

import (
	"fmt"
	"tutorial/library"
)

func propertyMethod() {
	library.SayHello()
	// library.introduce("oentoro")

	var s1 = library.Student{"Caisar Oentoro", 88}
	fmt.Println("Nama murid", s1.Name)
}
