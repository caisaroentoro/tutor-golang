package main

import "fmt"

func structure() {
	var s1 student
	s1.name = "Caisar Oentoro"
	s1.grade = 100

	s2 := student{grade: 99, name: "Caisar Oentoro"}

	fmt.Println("name \t: ", s1.name)
	fmt.Println("grade\t: ", s1.grade)

	fmt.Println("name: \t", s2.name)
	fmt.Println("grade: \t", s2.grade)

}
